% LaTeX preamble template for long-form documents
% Marius Cramer latex@mcramer.eu
% modified 2024-01-31


% PACKAGES
%% TEXT
\usepackage[utf8]{inputenc}        % UTF8 encoding
\usepackage[nottoc]{tocbibind}
\usepackage[ngerman]{babel}        % n(ew) german language support
\usepackage{csquotes}
\usepackage{blindtext}             % german lorem ipsum
\usepackage{textcomp}              % text companion fonts

%% ESSENTIAL
\usepackage[
    backend=biber,
    citestyle=alphabetic,
    bibstyle=alphabetic,
    % sorting=anyt,                % alphabetic name, year, title sorting
    sorting=none,                  % sort as cited
    giveninits=true                % shortened given name, written-out family name
    ]{biblatex}                    % bibliography management
\usepackage{geometry}              % typesetting geometry
\usepackage{url}                   % url support
\usepackage{graphicx}              % advanved graphics/figures support
\usepackage{float}                 % improved floating object interface
\usepackage[
    intlimits                      % place limits above/below integral
    ]{amsmath}                     % American Mathematical Society (AMS) math package
\usepackage{
    mathtools,                     % amsmath extension
    amsthm,                        % amsmath extension
    amssymb,                       % AMS symbols
}
\usepackage{mdframed}              % for box around Definition, Theorem, \ldots
\usepackage{fancyhdr}              % headers and footers
\usepackage{enumitem}              % advanced enumerated and itemized lists
\usepackage{wrapfig}               % wrapped figures
\usepackage{siunitx}               % SI units
\usepackage{eurosym}               % euro symbol
\usepackage{setspace}              % line heights

%% OPTIONAL
% \usepackage{subfig}
% \usepackage{rotating}            % rotated images
% \usepackage{listings}            % code support
% \usepackage{caption}
% \usepackage{pdfpages}
% \usepackage{tabularx}
% \usepackage{import}
% \usepackage{booktabs}            % advanced rule
% \usepackage{gensymb}
% \usepackage{adjustbox}
% \usepackage{parskip}
% \usepackage{datetime}

%% ALWAYS LOAD FINAL
\usepackage{hyperref}              % advanved hypertext support


% CONFIG
%% LINK COLORS
\hypersetup{
    colorlinks,
    citecolor=orange,
    linkcolor=dunkelgruen,
    urlcolor=dunkelblau
}

%% MARGINS, SPACING, PADDING
\geometry{
    a4paper,
    top=25mm,
    bottom=20mm,
    left=25mm,
    right=25mm,
    headsep=5em
}

%% HEADERS\FOOTERS
\fancyhf{}                         % clear headers and footers
\pagestyle{fancy}

%%% frontmatter header/footer style
\fancypagestyle{frontmatter}{%
    \fancyhf{}%
    \renewcommand{\headrulewidth}{0mm}%
    \renewcommand{\footrulewidth}{0mm}%
    \fancyfoot[C]{\thepage}%
}

%%% body header/footer style
\fancypagestyle{body}{%
    \fancyhf{}%
    \renewcommand{\headrulewidth}{0.1mm}%
    \renewcommand{\footrulewidth}{0.1mm}%
    \fancyhead[RO,LE]{\includegraphics[scale=0.25]{src/logo}}           % right
    \fancyhead[RE,LO]{MEIK2 A-14: Füllstandsregelung\\ \vspace{0.6em}}  % left
    \fancyhead[C]{}                                                     % center

    \fancyfoot[RO,LE]{\thepage}                                         % current page
    \fancyfoot[RE,LO]{\leftmark}                                        % current section
}

%% FIGURE SUPPORT
% \newcommand{\incfig}[1]{%
%   \def\svgwidth{\columnwidth}
%   \import{./src/figures/}{#1.pdf_tex}
% }

% CUSTOM DEFINITIONS
%% horizontal rule
% \newcommand\horizontalrule{
%   \noindent\rule[0.5ex]{\linewidth}{0.5pt}
% }

%% THEOREM ENVIRONMENTS
% \makeatother
%   \mdfsetup{skipabove=1em,skipbelow=0em}
%   \theoremstyle{definition}
%   \newmdtheoremenv[nobreak=true]{property}{Eigenschaft}
%   \newmdtheoremenv[nobreak=true]{proposition}{Vorschlag}
%   \newmdtheoremenv[nobreak=true]{thesis}{These}
%   \newmdtheoremenv{conclusion}{Konklusion}
%   \newtheorem*{observation}{Beobachtung}
%   \newtheorem*{remark}{Bemerkung}
%   \newtheorem*{problem}{Problem}
%   \newtheorem*{terminology}{Terminologie}
% \makeatletter
%
% \def\thm@space@setup{%
%   \thm@preskip=\parskip \thm@postskip=0pt
% }

%% PDF fixes (comment out for xelatex/lualatex)
\pdfsuppresswarningpagegroup=1
\pdfminorversion=7

%% table of contents lines
% \renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}     % dotted lines in ToC
% \renewcommand{\cftdot}{}                                  % no dots at all

%% HSB COLOR PALETTE
\definecolor{dunkelblau}{RGB}{15, 85, 141}
\definecolor{hellgrau}{gray}{0.92}
\definecolor{dunkelgrau}{gray}{0.3}
\definecolor{hellblau}{RGB}{49, 180, 200}
\definecolor{dunkelgruen}{RGB}{0, 145, 88}
\definecolor{hellgruen}{RGB}{107, 166, 61}
\definecolor{gelb}{RGB}{250, 190, 4}
\definecolor{orange}{RGB}{243, 119, 31}
\definecolor{rot}{RGB}{197, 7, 50}
\definecolor{lila}{RGB}{118, 100, 167}

%% clear frontmatter (if custom cover.tex is used)
\title{}
\date{}

%% bibliography source
\addbibresource{biblio.bib}

%% family name, given name
\DeclareNameAlias{sortname}{family-given}
\DeclareNameAlias{default}{family-given}

%% custom counter for switching page numbering types
\newcounter{alteSeitenzahl}

%% small size, bold face caption font
% \captionsetup{font={stretch=1.25}}
% \captionsetup[figure]{font=footnotesize, labelfont=bf}
% \captionsetup[table]{font=footnotesize, labelfont=bf}

%% custom submission date variable
% \newdate{abgabe}{day}{month}{year}

%% SI unit config
\sisetup{locale = DE}

%% increase table padding
\renewcommand{\arraystretch}{1.3}

%% count figure/table index per section for long documents with lots of figures
% \counterwithin{figure}{section}
% \counterwithin{table}{section}

%% increase padding between index and caption in listoffigures
% \makeatletter
% \def\l@figure{\@dottedtocline{1}{1.5em}{3em}}
% \makeatother

%% bibliography definition for multiple authors: et al.
\DefineBibliographyStrings{ngerman}{
    andothers = {{et\,al\adddot}},
}

%% autoref naming scheme
\addto\extrasngerman{
    \def\sectionautorefname{Kapitel}
    \def\subsectionautorefname{Abschnitt}
    \def\subsubsectionautorefname{Unterabschnitt}
    \def\equationautorefname{Gleichung}
    \def\figureautorefname{Abbildung}
    \def\tableautorefname{Tabelle}
}

%% custom autoref names
\addto\extrasngerman{}


%% unnumbered footnote command
\newcommand\blfootnote[1]{%
    \begingroup
    \renewcommand\thefootnote{}\footnote{#1}%
    \addtocounter{footnote}{-1}%
    \endgroup
}

%% fix overflowing bibliography lines
\appto{\bibsetup}{\sloppy}

%% indent depth (uncomment for no paragraph indent)
% \parindent=3ex

%% PRINT Definition
% \def\blankpage{%
%       \clearpage%
%       \thispagestyle{empty}%
%       \addtocounter{page}{-1}%
%       \null%
%       \clearpage}


%% OTHER CUSTOM PER-DOCUMENT DEFINITIONS BELOW
